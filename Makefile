CFLAGS = -Wall -g -x c++
.PHONY : all rebuild clean mrproper

############## BINARY ##############

tempcontrol: serial.o modbus.o main.o crc.o abstract_regulator.o rkc_cb100.o linkam.o lakeshore.o
	@echo "*** Linking all main objects files..."
	@g++ -lm modbus.o serial.o main.o crc.o abstract_regulator.o rkc_cb100.o linkam.o lakeshore.o -o tempcontrol

############## OBJECTS ##############

main.o: main.cpp serial.h modbus.h rkc_cb100.h abstract_regulator.h
	@echo "*** Compiling main.o"
	@g++ $(CFLAGS) -c main.cpp -o main.o

crc.o: crc.c crc.h
	@echo "*** Compiling crc.o"
	@g++ $(CFLAGS) -c crc.c -o crc.o

serial.o: serial.cpp serial.h
	@echo "*** Compiling serial.o"
	@g++ $(CFLAGS) -c serial.cpp -o serial.o

modbus.o: modbus.cpp modbus.h serial.h
	@echo "*** Compiling modbus.o"
	@g++ $(CFLAGS) -c modbus.cpp -o modbus.o

abstract_regulator.o: abstract_regulator.cpp abstract_regulator.h
	@echo "*** Compiling abstract_regulator.o"
	@g++ $(CFLAGS) -c abstract_regulator.cpp -o abstract_regulator.o

rkc_cb100.o: rkc_cb100.cpp rkc_cb100.h serial.h modbus.h abstract_regulator.h
	@echo "*** Compiling rkc_cb100.o"
	@g++ $(CFLAGS) -c rkc_cb100.cpp -o rkc_cb100.o

linkam.o: linkam.cpp linkam.h serial.h abstract_regulator.h
	@echo "*** Compiling linkam.o"
	@g++ $(CFLAGS) -c linkam.cpp -o linkam.o

lakeshore.o: lakeshore.cpp lakeshore.h serial.h abstract_regulator.h
	@echo "*** Compiling lakeshore.o"
	@g++ $(CFLAGS) -c lakeshore.cpp -o lakeshore.o

############## PHONY ##############
all: tempcontrol
rebuild: mrproper all

clean:
	@echo "*** Erasing objects files..."
	@rm -f *.o

mrproper: clean
	@echo "*** Erasing executables..."
	@rm -f tempcontrol serialtest
