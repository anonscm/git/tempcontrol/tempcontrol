#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

class TempControl:
	def __init__(self, path="."):
		self.path = path
		self.bin = "./tempcontrol"
		self.statefile = self.path+"/tempcontrol.state"
		self.readState()

	def readState(self):
		self.lastLogValues = {}

		try:
			self.state = open(self.statefile).read().split("\n")
			self.logfile = self.path+"/"+self.state[12]
			log = open(self.logfile).read().strip().split("\n")

			for k, v in zip(log[0].split("\t"), log[-1].split("\t")):
				self.lastLogValues[k] = v
		except Exception as e:
			print(str(e))

		try:    self.sp = float(self.lastLogValues['New SP (C)'])
		except: self.sp = float("nan")
		try:    self.pv = float(self.lastLogValues['PV (C)'])
		except: self.pv = float("nan")
		try:    self.pv2 = float(self.lastLogValues['PV2 (C)'])
		except: self.pv2 = float("nan")

	def wizard(self):
		r = os.system("cd %s; %s wizard" % (self.path, self.bin))
		self.readState()
		return r

	def next(self):
		r = os.system("cd %s; %s next" % (self.path, self.bin))
		self.readState()
		return r

	def stop(self):
		r = os.system("cd %s; %s stop" % (self.path, self.bin))
		self.readState()
		return r

def main():
	tempControl = TempControl()
	r = tempControl.wizard()
	while r == 0:
		print(tempControl.sp, tempControl.pv, tempControl.pv2)

		# do your measurements here!
		# last logfile entry values are in tempControl.lastLogValues

		r = tempControl.next()

if __name__ == '__main__':
	main()
