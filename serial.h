/*
   Name:            serial.h
   Author:          RJS
   Copyright:       GPL v3
   Description:     A little library allowing access to serial ports the same way in linux and windows
   Date:            08/06/08 (first windows version)
                    14/09/09 (now the linux adaptation)
*/

#ifndef SERIAL_H
#define SERIAL_H

#ifdef WIN32
// windows
#include <windows.h>
#define FDTYPE HANDLE
#else
// linux
#define FDTYPE int
#define DCB int
#endif

// generic class for virtual things

#ifndef __cplusplus
#error "Please compile in C++!"
#endif

class serial {
	public:
		serial(void);
		serial(const char*, unsigned int);
		~serial();
		int open(const char*, unsigned int, int, char, int);
		int open(const char*, unsigned int);
		int isopened(void);
		void close(void);
		int send(char);
		int send(const char*);
		int send(const char*, size_t);
		unsigned int nbqueue(void);
		char getc(void);
		unsigned int receive(char*, size_t);
		unsigned int binreceive(unsigned char*, size_t);
		void clear_buffer();
		int rts(int);
		int dtr(int);
		int brk(int);

	private:
		FDTYPE fd;
		DCB myDCB;
		int opened;
};

#endif
