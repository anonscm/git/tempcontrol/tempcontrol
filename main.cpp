/*
	TEMPCONTROL
	Copyright CNRS UMR 8181 (2016)

	romain.jooris@univ-lille.fr

	Simple program for remote control the temperature setpoint of PIDs
	or devices, to be used in scripts (or any scriptable softwares).

	This software is governed by the CeCILL license under French law and
	abiding by the rules of distribution of free software.  You can  use,
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info/".

	As a counterpart to the access to the source code and rights to copy,
	modify and redistribute granted by the license, users are provided only
	with a limited warranty  and the software's author, the holder of the
	economic rights,  and the successive licensors  have only limited
	liability.

	In this respect, the user's attention is drawn to the risks associated
	with loading,  using,  modifying and/or developing or reproducing the
	software by the user in light of its specific status of free software,
	that may mean  that it is complicated to manipulate,  and  that  also
	therefore means  that it is reserved for developers  and  experienced
	professionals having in-depth computer knowledge. Users are therefore
	encouraged to load and test the software's suitability as regards their
	requirements in conditions enabling the security of their systems and/or
	data to be ensured and,  more generally, to use and operate it in the
	same conditions as regards security.

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms.
*/

#define LOGFILENAMEFORMAT "%Y-%m-%d_%Hh%M.txt"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h> // mktime(), stat()
#include <string.h>
#include <unistd.h>
#include <time.h>  // time()
#include <ctype.h> // toupper()

#ifdef WIN32
	#include <windows.h>
	#define delay_ms(ms) Sleep(ms)
	#define sleep(s) Sleep(1000*s)
	#include <io.h>
	#define mkdir(dir, mode) mkdir(dir)

	#define CONFIG_FILE "c:\\data\\tempcontrol.conf"
	#define STATE_FILE  "c:\\data\\tempcontrol.state"
void winweeping(void) {
	fprintf(stderr, "[Press Control+C to exit the program]");
	for(;;) {
		Sleep(1000);
		fprintf(stderr, "\a");
	}
}
#else
	#include <unistd.h>
	#define delay_ms(ms) usleep(1000*ms)
	#define CONFIG_FILE "tempcontrol.conf"
	#define STATE_FILE  "tempcontrol.state"
void winweeping(void) { ; }
#endif

#include "modbus.h"
#include "rkc_cb100.h"
#include "linkam.h"
#include "lakeshore.h"

#define ISNAN(x) (x != x)

char* malloc_me_localtime(const time_t timestamp, const char *prefix, const char *format) {
	char buf[2000], *ptr = NULL;
	strftime(buf, sizeof buf, format, localtime(&timestamp));
	ptr = (char*)malloc(strlen(prefix) + strlen(buf) + 1);
	if (ptr) {
		strcpy(ptr, prefix);
		strcat(ptr, buf);
	}
	return ptr;
}


int readparam_string(FILE *fd, char** var) {
	char *p = NULL;
	char buf[300];
	size_t len;
	if(!fgets(buf, sizeof buf, fd)) {
		*var = NULL;
		return 1;
	}
	len = strlen(buf);
	*var = (char*)malloc(len+2);
	if (!*var) { return 2; }
	strncpy(*var, buf, len+1);

	p = *var;
	while (*p) {
		if (*p==0x0a || *p==0x0d) { *p='\0'; break; }
		p++;
	}
	return 0;
}

int readparam_float(FILE *fd, double* var) {
	char buf[300];
	if(!fgets(buf, sizeof buf, fd)) {
		return 1;
	}
	*var = atof(buf);
	return 0;
}

int readparam_int(FILE *fd, int* var) {
	char buf[300];
	if(!fgets(buf, sizeof buf, fd)) {
		return 1;
	}
	*var = atoi(buf);
	return 0;
}

int readparam_long(FILE *fd, long* var) {
	char buf[300];
	if(!fgets(buf, sizeof buf, fd)) {
		return 1;
	}
	*var = atol(buf);
	return 0;
}

typedef struct {
	char *serialport;
	char *logdir;
} conf_t;

int loadconf(const char* filename, conf_t *conf) {
	FILE *fd;
	fd = fopen(filename, "r");
	int err=0;

	if (!fd) { perror(filename); return -1; }
	err|= readparam_string(fd, &conf->serialport);
	err|= readparam_string(fd, &conf->logdir);
	fclose(fd);
	return err;
}

typedef enum tempconfiguration_t {
	DEFAULT = 0,
	RKC_CB100_REGULATOR,
	RKC_CB100_REGULATOR_AND_READER,
	LINKAM_T9X_STAGE,
	LAKESHORE_CONTROLLER
} tempconfiguration_t;

char tempconfiguration_t_helper[][60] = {
	"Nothing defined yet!",
	"One RKC CB100 PID controller",
	"Two RKC CB100 PID as a controller and a temperature reader",
	"Linkam T9x temperature controlled stage",
	"Lake Shore temperature controller",
	(char)NULL // end of list!
};

typedef struct {
	double lastsp;
	double min;
	double max;
	double step;
	double ramp_rate;
	double wait_time;
	int dir;
	int dir_of_ending_sweep;
	int sweep_cycles;
	int remaining_sweep_cycles;
	int tempcount;
	int cut_power_when_we_stop;
	char *logfile;
	time_t begin_time;
	tempconfiguration_t tempconfiguration;
} state_t;

int loadstate(const char* filename, const conf_t *conf, state_t *state) {
	FILE *fd;
	fd = fopen(filename, "r");
	int err=0;

	state->begin_time = 0;

	if (!fd) {
		memset(state, 0, sizeof(state_t));
		perror(filename);
		err = -1;
	}
	else {
		err|= readparam_float(fd, &state->lastsp);
		err|= readparam_float(fd, &state->min);
		err|= readparam_float(fd, &state->max);
		err|= readparam_float(fd, &state->step);
		err|= readparam_float(fd, &state->ramp_rate);
		err|= readparam_float(fd, &state->wait_time);
		err|= readparam_int(fd, &state->dir);
		err|= readparam_int(fd, &state->dir_of_ending_sweep);
		err|= readparam_int(fd, &state->sweep_cycles);
		err|= readparam_int(fd, &state->remaining_sweep_cycles);
		err|= readparam_int(fd, &state->tempcount);
		err|= readparam_int(fd, &state->cut_power_when_we_stop);
		readparam_string(fd, &state->logfile);
		readparam_long(fd, &state->begin_time);
		err|= readparam_int(fd, (int*)&state->tempconfiguration);
		fclose(fd);
	}

	if (err) {
		state->lastsp = 0;
		state->min = 0;
		state->max = 0;
		state->step = 0;
		state->dir = 0;
		state->dir_of_ending_sweep = 0;
		state->sweep_cycles = 0;
		state->remaining_sweep_cycles = 0;
		state->tempcount = 0;
		state->cut_power_when_we_stop = 1;
		state->tempconfiguration = DEFAULT;
	}

	if (!state->logfile) {
		state->logfile = malloc_me_localtime(time(NULL), conf->logdir, LOGFILENAMEFORMAT); //now
	}

	return err;
}

int savestate(const char* filename, state_t *state) {
	FILE *fd;
	fd = fopen(filename, "w");
	if (!fd) {
		perror(filename);
		return -1;
	}

	fprintf(fd, "%f\n",  state->lastsp);
	fprintf(fd, "%f\n",  state->min);
	fprintf(fd, "%f\n",  state->max);
	fprintf(fd, "%f\n",  state->step);
	fprintf(fd, "%f\n",  state->ramp_rate);
	fprintf(fd, "%f\n",  state->wait_time);
	fprintf(fd, "%d\n",  state->dir);
	fprintf(fd, "%d\n",  state->dir_of_ending_sweep);
	fprintf(fd, "%d\n",  state->sweep_cycles);
	fprintf(fd, "%d\n",  state->remaining_sweep_cycles);
	fprintf(fd, "%d\n",  state->tempcount);
	fprintf(fd, "%d\n",  state->cut_power_when_we_stop);
	fprintf(fd, "%s\n",  state->logfile);
	fprintf(fd, "%ld\n", state->begin_time);
	fprintf(fd, "%d\n",  state->tempconfiguration);
	fclose(fd);
	return 0;

}

int freeconf(conf_t *conf) {
	free(conf->serialport); conf->serialport = NULL;
	free(conf->logdir);     conf->logdir     = NULL;
	return 0;
}

int freestate(state_t *state) {
	free(state->logfile);   state->logfile   = NULL;
	return 0;
}

void help(conf_t *conf, state_t *state) {
	fprintf(stderr, "TEMPCONTROL - RJS UCCS CNRS 8181\n");
	fprintf(stderr, "Build date: %s %s\n\n", __DATE__, __TIME__);
	fprintf(stderr, "Simple program for changing the setpoint of RKC CB100 temperature controllers\n");
	fprintf(stderr, "for automation of experiments.\n\n");
	fprintf(stderr, "Usage:   tempcontrol wizard\n");
	fprintf(stderr, "         tempcontrol next\n");
	fprintf(stderr, "         tempcontrol next\n");
	fprintf(stderr, "         ...\n\n");
	fprintf(stderr, "Others args:\n");
	fprintf(stderr, "         tempcontrol [debug] [read] [sp <temperature>]\n");
	fprintf(stderr, "                     [min <Tmin>] [max <Tmax>] [step <delta temp>] [up] [down]\n");
	fprintf(stderr, "                     [ramp_rate <deg/min or 0 for instant>] [wait_time <min>]\n");
	fprintf(stderr, "                     You can set wait_time to -1 to don't even wait PV to reach SP.\n");
	fprintf(stderr, "                     \n");
	fprintf(stderr, "Example: tempcontrol min 100 max 750 step 25 sp 500 down\n");
	fprintf(stderr, "         tempcontrol next\n");
	fprintf(stderr, "         ...\n");
	fprintf(stderr, "Configuration file: %s\n", CONFIG_FILE);
	fprintf(stderr, "Log directory (from configuration file): %s\n", conf->logdir);
	fprintf(stderr, "Log file (from state file): %s\n", state->logfile);
}

int wiz_askvalue(const char *msg, double *value) {
	char buf[300];
	fprintf(stderr, "%s [%.2f] : ", msg, *value);
	fgets(buf, sizeof buf, stdin);
	if (buf[0] == '\n' || buf[0] == '\r') {
		return 1; // just enter = we keep the defaut value.
	}
	*value = atof(buf);
	return 0;
}

int wiz_askvalue(const char *msg, int *value) {
	char buf[300];
	fprintf(stderr, "%s [%d] : ", msg, *value);
	fgets(buf, sizeof buf, stdin);
	if (buf[0] == '\n' || buf[0] == '\r') {
		return 1; // just enter = we keep the defaut value.
	}
	*value = atoi(buf);
	return 0;
}

char wiz_askchar(const char *msg, const char *choices, char defaultchoice) {
	char buf[30];
	int i, p=10;
	for (;;) {
		fprintf(stderr, "%s [", msg);

		i = 0;
		while (choices[i]) {
			fprintf(stderr, "%s%c", i?"/":"", choices[i] == defaultchoice?toupper(choices[i]):tolower(choices[i]));
			i++;
		}
		fprintf(stderr, "] : ");
		fgets(buf, sizeof buf, stdin);

		if (defaultchoice && (buf[0] == '\n' || buf[0] == '\r')) {
			return defaultchoice; // just enter ? we keep the defaut value.
		}

		i=0;
		while (choices[i]) {
			if (tolower(buf[0]) == tolower(choices[i])) {
				return choices[i];
			}
			i++;
		}
		if (!p) { fprintf(stderr, "Gnahhh! Are you actually dumb?\n"); p=10; }
		else {
			p--;
			fprintf(stderr, "Type ");
			i = 0;
			while (choices[i]) {
				if (i) { fprintf(stderr, " or "); }
				fprintf(stderr, "\"%c\"", toupper(choices[i]));
				i++;
			}
			fprintf(stderr, "!\n");
		}
	}
	return buf[0];
}


double calculate_next_temperature_point(state_t *state) {
	double newsp = state->lastsp + state->dir * fabs(state->step);

	if (state->sweep_cycles && !state->remaining_sweep_cycles) {
		return nan("");
	}

	if (state->dir>=0) {
		if (newsp >= state->max) {
			newsp = state->max;
			state->dir = -1;
			if (state->dir_of_ending_sweep >= 0 && state->sweep_cycles) {
				state->remaining_sweep_cycles--;
			}
		}
	}
	else {
		if (newsp <= state->min) {
			newsp = state->min;
			state->dir = 1;
			if (state->dir_of_ending_sweep <= 0 && state->sweep_cycles) {
				state->remaining_sweep_cycles--;
			}
		}
	}
	return newsp;
}

void simulatecycles(const state_t *realstate) {
	state_t dummystate;
	double simsp, olddir=0;
	int chdir=0;
	memcpy(&dummystate, realstate, sizeof(state_t));
	fprintf(stderr, "\nTemperature points: \n%g", dummystate.lastsp);
	for(;;) {
		simsp = calculate_next_temperature_point(&dummystate);
		dummystate.lastsp = simsp;
		if (isnan(simsp)) {
            fprintf(stderr, "\tEND\n\n");
			break;
		}
		fprintf(stderr, "\t%g", simsp);
		if (olddir != dummystate.dir) {
            chdir++;
			if (!dummystate.sweep_cycles && chdir>3) {
                fprintf(stderr, "\t...etc.\n\n");
				break;
			}
			olddir = dummystate.dir;
		}
	}
}

double wizard(const conf_t *conf, state_t *state) {
	char a;
	int n;

	state->begin_time = time(NULL); // now!
	fprintf(stderr, "\n*** TEMPCONTROL INITIAL CONFIGURATION WIZARD - RJS UCCS %s ***\a\n", __DATE__);
	do {
		fprintf(stderr, "\nPlease choose:\n");
		for (n=1; tempconfiguration_t_helper[n][0]; n++) {
			fprintf(stderr, "%2d: %s\n", n, tempconfiguration_t_helper[n]);
		}
		fprintf(stderr, "\n");
		wiz_askvalue("What regulator-box type are you using?", (int*)&state->tempconfiguration);
		wiz_askvalue("Minimum setpoint temperature (C)", &state->min);
		wiz_askvalue("Maximum setpoint temperature (C)", &state->max);
		wiz_askvalue("Temperature steps (C)", &state->step);
		wiz_askvalue("Initial temperature (C)", &state->lastsp);
		if (state->lastsp <= state->min) { state->dir = 1; }
		else if (state->lastsp >= state->max) { state->dir = -1; }
		else {
			switch (wiz_askchar("Initially increase or decrease temperature?", "id", state->dir==1?'i':'d')) {
				case 'i':
				default:
					state->dir = 1;
					break;

				case 'd':
					state->dir = -1;
					break;
			}
		}

		wiz_askvalue("How many cycles do you want to do? (type 0 to never stop)", &state->sweep_cycles);
		state->remaining_sweep_cycles = 0;
		if (state->sweep_cycles) {
			state->remaining_sweep_cycles = state->sweep_cycles;
			switch (wiz_askchar("Stop when the temperature is High or Low?", "hl", state->dir_of_ending_sweep==1?'h':'l')) {
				case 'h':
					state->dir_of_ending_sweep = 1;
					break;
				case 'l':
					state->dir_of_ending_sweep = -1;
					break;
			}

			switch (wiz_askchar("Let the oven cool down to ambiant once finished?", "yn", state->cut_power_when_we_stop?'y':'n')) {
				case 'y':
					state->cut_power_when_we_stop = 1;
					break;
				case 'n':
					printf("DON'T FORGET IT HOT PLEASE!\n");
					state->cut_power_when_we_stop = 0;
					break;
			}
		}

		wiz_askvalue("Temperature ramp rate (C/min or 0 for instant changes)", &state->ramp_rate);
		wiz_askvalue("Wait time for stabilizing (min)", &state->wait_time);
		simulatecycles(state);
		a = wiz_askchar("Are the parameters correct?", "yn", 'n');
	} while (a != 'y');

	state->tempcount = 0;
	free(state->logfile);
	state->logfile = malloc_me_localtime(time(NULL), conf->logdir, LOGFILENAMEFORMAT); //now
	fprintf(stderr, "New log file: %s\n", state->logfile);
	mkdir(conf->logdir, 0777);
	return state->lastsp;
}

void fprintf_localtime(FILE *fd, time_t time) {
	char buf[100];
	strftime(buf, sizeof buf, "%Y-%m-%d %H:%M:%S", localtime(&time));
	fprintf(fd, "%s", buf);
}

void fprint_double(FILE* fd, const double &value) {
	if (isnan(value)) {
        fprintf(fd, "\tNaN");
	}
	else {
		fprintf(fd, "\t%.2f", value);
	}
}

int write_to_logfile(const char *filename, double t, unsigned int count, double oldsp, double newsp, double pv_oven, double pv_samples, const char *msg) {
	FILE *fd;
	struct stat s;
	int r;

	r = stat(filename, &s);
	fd = fopen(filename, "a");
	if (!fd) {
		perror(filename);
		return -1;
	}
	if (r == -1) { // it will be first entry in the file, writing colomns headers !
		fprintf(fd, "Point #\t");
		fprintf(fd, "t (s)\t");
		fprintf(fd, "Date & Time\t");
		fprintf(fd, "Old SP (C)\tNew SP (C)\tPV (C)\tPV2 (C)\n");
	}
	fprintf(fd, "%u\t%.0f\t", count, t);
	fprintf_localtime(fd, time(NULL));
	fprint_double(fd, oldsp);
	fprint_double(fd, newsp);
	fprint_double(fd, pv_oven);
	fprint_double(fd, pv_samples);

	if (msg) { fprintf(fd, "\t%s", msg); } // got something to say?
	fprintf(fd, "\n");
	fclose(fd);
	return 0;
}

int main(int argc, char **argv) {
	modbus *modbus_comm = NULL;
	serial *serial_comm = NULL;
	abstract_regulator *tempregulator = NULL, *tempreader = NULL;

	conf_t conf;
	state_t state;
	int nostate = 1;
	int writelog = 0;
	int debug=0;

	if (loadconf(CONFIG_FILE, &conf)) {
		fprintf(stderr, "Unable to read the config file (%s).\a\n", CONFIG_FILE);
		winweeping();
		return 1;
	}
	if (loadstate(STATE_FILE, &conf, &state)) {
		fprintf(stderr, "Unable to read the state file (%s).\a\n", STATE_FILE);
		nostate = 1;
	}
	else {
		nostate = 0;
	}

	if (argc<2) {
		help(&conf, &state);
	}

	double oldsp = nan(""), newsp = nan(""), pv_oven = nan(""), pv_samples = nan("");

	int finished=0, stopnow=0, act=0;
	int i=0;
	while (argv[++i]) {
		if (!strcasecmp(argv[i], "read")) {
			writelog = 1;
		}
		else if (!strcasecmp(argv[i], "debug")) {
			debug=1;
		}
		else if (!strcasecmp(argv[i], "ramp_rate")) {
			if (!argv[i+1]) { fprintf(stderr, "ERROR: %s need an argument!\a\n", argv[i]); }
			else {
				state.ramp_rate = atof(argv[++i]);
			}
		}
		else if (!strcasecmp(argv[i], "wait_time")) {
			if (!argv[i+1]) { fprintf(stderr, "ERROR: %s need an argument!\a\n", argv[i]); }
			else {
				state.wait_time = atof(argv[++i]);
			}
		}
		else if (!strcasecmp(argv[i], "sp")) {
			if (!argv[i+1]) { fprintf(stderr, "ERROR: %s need an argument!\a\n", argv[i]); }
			else {
				newsp = atof(argv[++i]);
				act=1;
			}
		}
		else if (!strcasecmp(argv[i], "up")) {
			state.dir=1;
		}
		else if (!strcasecmp(argv[i], "down")) {
			state.dir=-1;
		}
		else if (!strcasecmp(argv[i], "min")) {
			if (!argv[i+1]) { fprintf(stderr, "ERROR: %s need an argument!\a\n", argv[i]); }
			else {
				state.min = atof(argv[++i]);
			}
		}
		else if (!strcasecmp(argv[i], "max")) {
			if (!argv[i+1]) { fprintf(stderr, "ERROR: %s need an argument!\a\n", argv[i]); }
			else {
				state.max = atof(argv[++i]);
			}
		}
		else if (!strcasecmp(argv[i], "step")) {
			if (!argv[i+1]) { fprintf(stderr, "ERROR: %s need an argument!\a\n", argv[i]); }
			else {
				state.step = atof(argv[++i]);
			}
		}
		else if (!strcasecmp(argv[i], "wizard")) {
			newsp = wizard(&conf, &state);
			savestate(STATE_FILE, &state);
			act=1;
		}
		else if (!strcasecmp(argv[i], "stop")) {
			stopnow=1;
		}
		else if (!strcasecmp(argv[i], "next")) {
			act=1;
			if (nostate) {
				fprintf(stderr, "ERROR: you can't go to the next temperature if the state is unknow!\a\n");
				winweeping();
			}
			else {
				if (state.dir == 0) { state.dir = 1; } // raise temp by default
				newsp = calculate_next_temperature_point(&state);
				if (isnan(newsp)) { finished = 1; }
			}
		}
		else if (strstr(argv[i], "\\tempcontrol.exe")) {
			// suppress warning message when EC-LAB is so dumb it pass the
			// program's path and name as an argument to the program. Tsss...
			fprintf(stderr, "Got \"%s\" as argument, silly EC-Lab.\n", argv[i]);
		}
		else {
			fprintf(stderr, "WARNING: %s not understood!\a\n", argv[i]);
		}
	}

	// ok, time to do things!
	switch (state.tempconfiguration) {
		case RKC_CB100_REGULATOR:
			modbus_comm = new modbus(conf.serialport, 19200);
			tempregulator = new rkc_cb100(modbus_comm, 1);
			break;

		case RKC_CB100_REGULATOR_AND_READER:
			modbus_comm = new modbus(conf.serialport, 19200);
			tempregulator = new rkc_cb100(modbus_comm, 1);
			tempreader = new rkc_cb100(modbus_comm, 2);
			break;

		case LINKAM_T9X_STAGE:
			serial_comm = new serial(conf.serialport, 19200);
			tempregulator = new linkam(serial_comm);
			break;

		case LAKESHORE_CONTROLLER:
			serial_comm = new serial();
			serial_comm->open(conf.serialport, 9600, 7, 'O', 1);
			tempregulator = new lakeshore(serial_comm);
			tempreader = new lakeshore(serial_comm, 'B');
			break;

		default: // oh crap!
			fprintf(stderr, "Unknow temperature controller %d\n", state.tempconfiguration);
			winweeping();
			exit(1);
			break;
	}

	if (!tempregulator) { winweeping(); return 1; }

	if (tempregulator && !tempregulator->comm_isopened()) {
		fprintf(stderr, "Can't open %s!\a\n", conf.serialport);
		winweeping();
		return 1;
	}

	if (debug) {
		if (modbus_comm) { modbus_comm->enable_debug(); }
	}

	pv_oven = tempregulator->get_pv();
	oldsp   = tempregulator->get_sp();

	if (stopnow) { newsp = tempregulator->stop(); }
	else if (act) {
		if (finished) {
			writelog = 1;
			if (state.cut_power_when_we_stop) {
				newsp = tempregulator->stop();
			}
			else {
				newsp = tempregulator->get_sp();
			}
		}
		else if (!ISNAN(newsp)) {
			state.lastsp = newsp;
			state.tempcount++;
			savestate(STATE_FILE, &state);
			if ((state.tempcount == 1) && (pv_oven <= (newsp+1)) && (pv_oven >= (newsp-1)) && oldsp == newsp) {
				fprintf(stderr, "Already at the right temperature! Skipping first waiting time...\n");
			}
			else {
				newsp = tempregulator->ramp_and_wait(newsp, state.ramp_rate, state.wait_time);
			}
			if (isnan(newsp)) {
				winweeping();
				delete tempregulator;
				if (tempreader) delete tempreader;
				exit(EXIT_FAILURE);
			}

			pv_oven = tempregulator->get_pv();
			if (tempreader) { pv_samples = tempreader->get_pv(); }
			writelog = 1;
		}
		else {
			savestate(STATE_FILE, &state);
		}
	}

	if (tempregulator) { pv_oven = tempregulator->get_pv(); }
	if (tempreader)    { pv_samples = tempreader->get_pv(); }

	if (writelog) {
		write_to_logfile(state.logfile, difftime(time(NULL), state.begin_time), state.tempcount, oldsp, newsp, pv_oven, pv_samples, finished?"Finished":NULL);
	}

	printf("OldSP=%.2f\t", oldsp);
	printf("NewSP=%.2f\t", newsp);
	printf("OvenPV=%.2f\t", pv_oven);
	if (tempreader) printf("SmplPV=%.2f", pv_samples);
	printf("\n");
	if (tempregulator) delete tempregulator;
	if (tempreader)    delete tempreader;
	if (modbus_comm)   delete modbus_comm;
	if (serial_comm)   delete serial_comm;
	freeconf(&conf);
	freestate(&state);
	if (finished) {
		fprintf(stderr, "\n\n");
		fprintf(stderr, "     #######   ###   #     #   ###    #####  #     # ####### ######\n");
		fprintf(stderr, "     #          #    ##    #    #    #     # #     # #       #     #\n");
		fprintf(stderr, "     #          #    # #   #    #    #       #     # #       #     #\n");
		fprintf(stderr, "     #####      #    #  #  #    #     #####  ####### #####   #     #\n");
		fprintf(stderr, "     #          #    #   # #    #          # #     # #       #     #\n");
		fprintf(stderr, "     #          #    #    ##    #    #     # #     # #       #     #\n");
		fprintf(stderr, "     #         ###   #     #   ###    #####  #     # ####### ######\a\a\a\n\n");
		winweeping();
		return 1;
	}
	return 0;
}
