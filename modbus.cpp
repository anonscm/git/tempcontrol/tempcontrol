/*
  Name:        modbus.c
  Copyright:   UCCS CNRS 2016
  Author:      RJS
  Description: modbus class, using opensource win/unix serial class
  Date:        09/05/16 - first version
               30/03/17 - more delay between RTS enable and transmit (1 -> 30ms)
*/

#include <stdio.h>
#include <math.h>

#ifdef WIN32
	#include <windows.h>
	#define delay_s(s) Sleep(1000*s)
	#define delay_ms(ms) Sleep(ms)
#else
	#include <unistd.h>
	#define delay_s(s) sleep(s)
	#define delay_ms(ms) usleep(1000*ms)
#endif

#include "modbus.h"
#include "crc.h"

modbus::modbus(void) {
	debug=0;
}

modbus::modbus(const char *portname, unsigned int baud=9600) {
	open(portname, baud);
	debug=0;
}

void modbus::enable_debug(void) {
	fprintf(stderr, "Modbus debug mode enabled.\n");
	debug=1;
}
void modbus::disable_debug(void) {
	debug=0;
}

int modbus::request_one_time(const unsigned char *req, size_t bytes, unsigned char *buffer, size_t buffer_size) {
	unsigned int crc;
	char crc_lo, crc_hi;
	size_t rxbytes;

	crc = crc16((unsigned char*)req, bytes);
	crc_lo = (char)(crc%256);
	crc_hi = (char)(crc/256);

	clear_buffer();
	if (debug) {
		fprintf(stderr, "TX: ");
		for(size_t i=0; i<bytes; i++) {
			fprintf(stderr, "%02X%s", (unsigned char)req[i], (i%2?" ":""));
		}
		fprintf(stderr, "%02X%02X\n", (unsigned char)crc_lo, (unsigned char)crc_hi);
	}
	rts(1);
	delay_ms(30);
	send((char*)req, bytes);
	send(crc_lo);
	send(crc_hi);
	delay_ms(100);
	rts(0);
	delay_ms(200);

	if (buffer && buffer_size > 0) {
		rxbytes = binreceive((unsigned char*)buffer, buffer_size);
		if (debug) fprintf(stderr, "RX: ");
		if (!rxbytes) {
			if (debug) fprintf(stderr, "[NO RESPONSE]\n");
			return 1;
		}
		if (debug) {
			for(size_t i=0; i<rxbytes; i++) {
				fprintf(stderr, "%02X%s", (unsigned char)buffer[i], (i%2?" ":""));
			}
		}
		crc = crc16((unsigned char*)buffer, rxbytes);
		if (debug) fprintf(stderr, " [CRC %s]\n", crc?"FAIL":"OK");
		if (crc) { return 2; }
	}
	return 0;
}

int modbus::request(const unsigned char *req, size_t bytes, unsigned char *buffer, size_t buffer_size) {
	int i=10;
	int ret;
	int mefiance = 0;
	while(i) {
		ret = request_one_time(req, bytes, buffer, buffer_size);
		if (!ret) {
			if (!mefiance) { return 0; } // I trust you.
			else { mefiance--; } // Hmm. say it again?
		}
		else {
			if (ret!=1) { enable_debug(); } // error other than "no response" ? okay, let's debug that!
			fprintf(stderr, "Modbus error: %s, %s\n", (ret==1?"no response":"CRC failed"), --i?"retrying...":"giving up now.");
			mefiance = 2; // if the controller is just been powered up, its response could be very erroneous
		}
		delay_ms(3000);
	}
	return ret;
}
