/*
  Name:        lakeshore.cpp
  Copyright:   UCCS CNRS 2016
  Author:      RJS
  Date:        24/04/23 (start writing)
  Description: module for controlling a Lake Shore 325 temperature controller
*/

//~ #define DEBUG

#include <stdio.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#ifdef WIN32
	#include <windows.h>
	#define delay_s(s) Sleep(1000*s)
	#define delay_ms(ms) Sleep(ms)
	#define DEGSYMBOL "\xF8"
#else
	#include <unistd.h>
	#define delay_s(s)   sleep(s)
	#define delay_ms(ms) usleep(1000*ms)
	#define DEGSYMBOL "°"
#endif

#include "lakeshore.h"

lakeshore::lakeshore(serial* serial_connection) {
	commbus = serial_connection;
	lakeshore::channel = 'A';
	unit = 0; // 0: undefined / 1: Kelvin / 2: Celsius / 3 = Sensor Unit
}

lakeshore::lakeshore(serial* serial_connection, char channel) {
	commbus = serial_connection;
	if (channel == 'B' or channel == 'b') {
		lakeshore::channel = 'B';
	}
	else {
		channel = 'A';
	}
	unit = 0; // 0: undefined / 1: Kelvin / 2: Celsius / 3 = Sensor Unit
}

int lakeshore::comm_isopened(void) {
	return commbus->isopened();
}

double lakeshore::get_pv(void) {
	double val;
	char txbuf[64];
	int retries=2;

	snprintf(txbuf, sizeof txbuf, "CRDG? %c", channel); // A or B
	do {
		val = reqf(txbuf);
	}
	while (isnan(val) && retries--);
	return val;
}

double lakeshore::get_sp(void) {
	int retries=2;
	double val;

	do {
		if (!unit) { check_sp_unit(); }
		val = reqf("SETP?");
		if (!isnan(val)) {
			if (unit == 1) { return val - 273.15; }
			if (unit == 2) { return val; }
		}
	}
	while (retries--);
	return nan("");
}

double lakeshore::stop(void) {
	fprintf(stderr, "Reducing setpoint temperature to absolute zero...\n");
	set_sp(-273.15);
	fprintf(stderr, "Setting now the controller to power-up settings...\n");
	req("*RST");
	return get_sp();
}

double lakeshore::set_sp(double temperature) {
	return go_ramp(temperature, 0);
}

double lakeshore::ramp_and_wait(double final_sp, double ramp_rate, double wait_time) {
	double initial_pv = nan(""), pv=nan(""), sp=nan(""), deltaT=nan("");
	double elapsed_time;

	initial_pv = get_pv();
	time_t initial_time;

	if (isnan(initial_pv)) {
		fprintf(stderr, "ERROR: Could not read Lake Shore's PV temperature!\a\n");
		return nan("");
	}

	set_sp(initial_pv);

	if (ramp_rate ==  0) { ramp_rate =     0; }
	if (ramp_rate > 100) { ramp_rate =   100; }
	if (ramp_rate <   0) { ramp_rate =     0; }
	if (final_sp < -273.15) { final_sp  = -273.15; }

	fprintf(stderr, "Setting temperature from %.1f%sC to %.1f%sC", initial_pv, DEGSYMBOL, final_sp, DEGSYMBOL);
	fprintf(stderr, " (%.2f C/min)", ramp_rate);
	if (wait_time>0) { fprintf(stderr, " with %.1f minute(s) of settling time", wait_time); }
	fprintf(stderr, ".\n");
	sp = go_ramp(final_sp, ramp_rate);
	if (wait_time<0) { return final_sp; }

	initial_time = time(NULL);
	if (initial_time == ((time_t)-1)) {
		perror("Failure to obtain the current time");
		return final_sp;
	}

	for (;;) {
		sp = get_sp();
		deltaT = fabs(sp-final_sp);

		elapsed_time = difftime(time(NULL), initial_time) / 60;
		fprintf(stderr, "                \r");
		fprintf(stderr, "Ramping since %.2f minutes: SP=%.2f%sC (heating power=%.0f%%)", elapsed_time, sp, DEGSYMBOL, reqf("HTR?"));
		fprintf(stderr, ", %.2f%sC remaining...", deltaT, DEGSYMBOL);

		if (reqf("RAMPST?") == 0) { break; }
	}
	fprintf(stderr, " done.                \n");

	if (wait_time<0) { return final_sp; }

	initial_time = time(NULL);
	if (initial_time == ((time_t)-1)) {
		perror("Failure to obtain the current time");
		return final_sp;
	}

	do {
		pv = get_pv();
		if (isnan(pv)) {
			fprintf(stderr, "ERROR: Could not read controller PV temperature!\a\n");
			continue;
		}
		deltaT = fabs(pv-final_sp);
		elapsed_time = difftime(time(NULL), initial_time) / 60;
		fprintf(stderr, "\rWaiting PV=%.2f%sC to reach %.2f since %.2f minutes, power=%.0f%%...", pv, DEGSYMBOL, final_sp, elapsed_time, reqf("HTR?"));
		delay_ms(500);
	} while (deltaT > 0.1);
	fprintf(stderr, " ok.\n");

	initial_time = time(NULL);
	if (initial_time == ((time_t)-1)) {
		perror("Failure to obtain the current time");
		return sp;
	}

	do {
		delay_ms(2000);
		pv = get_pv();
		elapsed_time = difftime(time(NULL), initial_time) / 60;
		fprintf(stderr, "\rWaiting now %.0f minute(s). PV=%.2f%sC, power=%.0f%%, %.2f minute(s) remaining...", wait_time, pv, DEGSYMBOL, reqf("HTR?"), fmax(wait_time-elapsed_time, 0));

	} while ((wait_time-elapsed_time)>0);
	fprintf(stderr, " elapsed!\n");

	return final_sp;
}

lakeshore::~lakeshore() {
	//fprintf(stderr, "Lakeshore object destructed!\n");
}

/***** PRIVATE METHODS *****/

int lakeshore::communicate(const char *txstr, char *rxbuf, size_t buflen) {
	delay_ms(50);
	commbus->clear_buffer();
	commbus->send(txstr);
	commbus->send("\r\n");
	if (rxbuf) {
		delay_ms(100);
		if (commbus->receive(rxbuf, sizeof rxbuf) < 2) { return 0; }
#ifdef DEBUG
		printf("%s => %s\n", txstr, rxbuf);
#endif
		return strlen(rxbuf) - 2;
	}
	else {
#ifdef DEBUG
		printf("> %s\n", txstr);
#endif
	}
	return 0;
}

void lakeshore::req(const char *str) {
	communicate(str, NULL, 0);
}

double lakeshore::reqf(const char *str) {
	char rxbuf[100];
	if (!communicate(str, rxbuf, sizeof rxbuf)) {
		return nan("");
	}
	if (rxbuf[0] == ';') {
		return nan("");
	}
	return atof(rxbuf);
}

int lakeshore::check_sp_unit(void) {
	char rxbuf[100];
	if (!communicate("CSET? 1", rxbuf, sizeof rxbuf)) {
		fprintf(stderr, "\aERROR: no reply from Lake Shore controller!\n");
		return 0;
	}

	if (rxbuf[0] != channel) {
		fprintf(stderr, "\aChannel %c is not configured for loop 1 !!!\n", channel);
		return 0;
	}
	unit = atoi(&rxbuf[2]);
	return unit;
}

double lakeshore::go_ramp(double final_sp, double ramp_rate) {
	double td;
	char txbuf[64];
	if (!unit) { check_sp_unit(); }
	if (unit == 1) { td = 273.15; } // unit is talking Kelvin
	else if (unit == 2) { td = 0.0; } // we're both talking Celcius
	else { return nan(""); } // no.

	snprintf(txbuf, sizeof txbuf, "RAMP 1,1,%.1f", ramp_rate);
	req(txbuf);
	delay_ms(200);
	snprintf(txbuf, sizeof txbuf, "SETP 1,%f", final_sp+td);
	req(txbuf);
	delay_ms(200);
	return final_sp;
}
