/*
  Name:        linkam.cpp
  Copyright:   UCCS CNRS 2016
  Author:      RJS
  Date:        27/09/16 (start writing)
  Description: module for controlling the temperature of a linkam T95 stage
*/

#include <stdio.h>
#include <math.h>
#include <stdio.h>
#include <time.h>

#ifdef WIN32
	#include <windows.h>
	#define delay_s(s) Sleep(1000*s)
	#define delay_ms(ms) Sleep(ms)
	#define DEGSYMBOL "\xF8"
#else
	#include <unistd.h>
	#define delay_s(s)   sleep(s)
	#define delay_ms(ms) usleep(1000*ms)
	#define DEGSYMBOL "°"
#endif

#include "linkam.h"

linkam::linkam(serial* serial_connection) {
	commbus = serial_connection;
	linkam_status_byte=0; linkam_error_byte=0; linkam_pump_byte=0; linkam_gen_byte=0;
}

int linkam::comm_isopened(void) {
	return commbus->isopened();
}

double linkam::get_pv(void) {
	unsigned char rxbuf[11];
	double pv_temperature = nan("");
	linkam_status_byte = 0;

	delay_ms(100);
	commbus->clear_buffer();
	commbus->binreceive(rxbuf, sizeof rxbuf);
	commbus->send("T\r");
	delay_ms(100);
	if (!commbus->binreceive(rxbuf, sizeof rxbuf)) { return nan(""); }

	if (rxbuf[10] != '\r') { return nan(""); }
	linkam_status_byte = rxbuf[0];
	linkam_error_byte  = rxbuf[1];
	linkam_pump_byte   = rxbuf[2];
	linkam_gen_byte    = rxbuf[3];

	rxbuf[10] = '\0';
	pv_temperature = hex2dbl(&rxbuf[6])/10.0;
	return pv_temperature;
}

double linkam::get_sp(void) {
	double pv_temperature = get_pv();

	if (linkam_status_byte == 0x30) { return pv_temperature; }
	if (linkam_status_byte == 0x40) { return pv_temperature; }
	if (linkam_status_byte == 0x50) { return pv_temperature; }
	return nan("");
}

double linkam::stop(void) {
	commbus->send("E\r");
	fprintf(stderr, "Stopping regulating!\n");
	return get_sp();
}

double linkam::set_sp(double temperature) {
	return ramp_and_wait(temperature, 0, -1);
}

double linkam::ramp_and_wait(double final_sp, double ramp_rate, double wait_time) {
	double initial_pv = nan(""), pv=nan(""), sp=nan(""), deltaT=nan(""), deltaT_ref=nan("");
	//unsigned char rxbuf[11];
	double elapsed_time;
	int ok=0, ko=0;

	begin: // je sais, c'est moche

	initial_pv = get_pv();
	time_t initial_time;

	if (isnan(initial_pv)) {
		fprintf(stderr, "ERROR: Could not read Linkham's PV temperature!\a\n");
		return nan("");
	}


	if (ramp_rate ==  0   ) { ramp_rate =   99.99; }
	if (ramp_rate  > 99.99) { ramp_rate =   99.99; }
	if (ramp_rate  <  0.01) { ramp_rate =    0.01; }
	if (final_sp > 1500.00) { final_sp  = 1500.00; }
	if (final_sp < -196.00) { final_sp  = -196.00; }

	fprintf(stderr, "Setting temperature from %.1f%sC to %.1f%sC", initial_pv, DEGSYMBOL, final_sp, DEGSYMBOL);
	fprintf(stderr, " (%.2f C/min)", ramp_rate);
	if (wait_time>0) { fprintf(stderr, " with %.1f minute(s) of settling time", wait_time); }
	fprintf(stderr, ".\n");

     sp = go_ramp(final_sp, ramp_rate);
	if (wait_time<0) { return sp; }

	initial_time = time(NULL);
	if (initial_time == ((time_t)-1)) {
		perror("Failure to obtain the current time");
		return nan("");
	}

	ok=0; ko=0;
	do {
		delay_s(2);

		pv = get_pv();
		deltaT = fabs(pv-final_sp);
		if ((isnan(deltaT_ref)) || (deltaT<deltaT_ref)) {
			deltaT_ref = deltaT;
		}

		elapsed_time = difftime(time(NULL), initial_time) / 60;
		fprintf(stderr, "                \r");
		fprintf(stderr, "Ramping since %.2f minutes: PV=%.1f%sC", elapsed_time, pv, DEGSYMBOL);
		fprintf(stderr, ", %.0f%sC remaining", deltaT, DEGSYMBOL);
		if((deltaT-deltaT_ref)>0.1) {
			fprintf(stderr, ", %.1f%sC offroad", deltaT-deltaT_ref, DEGSYMBOL);
		}


		switch (linkam_status_byte) {
			case 0x10:
				fprintf(stderr, " (heating)");
				ok=0;
				break;

			case 0x20:
				fprintf(stderr, " (cooling)");
				ok=0;
				break;

			case 0x30:
				fprintf(stderr, " (SP reached)"); // OKAY!
				ok++;
				break;

			case 0x40:
				fprintf(stderr, " (holding time)");
				break;

			case 0x50:
				fprintf(stderr, " (holding temp)");
				break;

			default:
				ok=0;
				ko++;
				break;
		}
		fprintf(stderr, "...");

		if (ok<10) {
			if((deltaT-deltaT_ref)>2.0) { ko++; }
			if (ko>=5) { // did that damn controller forgot the setpoint we want ?
				ko=0;
				fprintf(stderr, " \a renewing the request.\n");
				sp = go_ramp(final_sp, ramp_rate);
				deltaT_ref = deltaT+1;
			}
		}

	}
	while (ok<10);
	fprintf(stderr, " done.                \n");

	initial_time = time(NULL);
	if (initial_time == ((time_t)-1)) {
		perror("Failure to obtain the current time");
		return sp;
	}

	do {
		delay_ms(2000);
		pv = get_pv();
		deltaT = fabs(pv-final_sp);
		if (deltaT>10) {
			fprintf(stderr, "\n\aPROBLEM: temperature out of control (read: %.1f%sC)\n", pv, DEGSYMBOL);
			goto begin;
		}
		elapsed_time = difftime(time(NULL), initial_time) / 60;
		fprintf(stderr, "\rWaiting now %.0f minute(s). %.2f minute(s) remaining...", wait_time, fmax(wait_time-elapsed_time, 0));

	} while ((wait_time-elapsed_time)>0);
	fprintf(stderr, " elapsed!\n");

	return sp;
}

linkam::~linkam() {
	//fprintf(stderr, "Linkham object destructed!\n");
}

/***** PRIVATE METHODS *****/

double linkam::go_ramp(double final_sp, double ramp_rate) {
	double sp;

	// added 2022-09-14 to "enable" the LN2 pump (its buggy controller seens to need a nudge or something)
	commbus->send("P9\r");
	delay_ms(100);
	commbus->send("P0\r");
	delay_ms(100);
	commbus->send("Pa0\r");
	delay_ms(100);

	dbl_cmd("R1", ramp_rate*100); // setting ramp rate
	delay_ms(100);
	sp = dbl_cmd("L1", final_sp*10)/10; // setting "limit" temperature
	delay_ms(100);
	commbus->send("S\r"); // start the ramp...
	return sp;
}


double linkam::hex2dbl(const unsigned char *hexstr) {
	unsigned int n;
	sscanf((const char*)hexstr, "%X", &n);
	if (n >= 0x8000) {
		return -(double)(0x8000 - (n%0x8000));
	}
	return (double) n;
}

signed int linkam::dbl_cmd(const char *command, double dbl) {
	unsigned int n;
	char buf[20];
	if (dbl<0)  { n=(unsigned int)round(-dbl); }
	else        { n=(unsigned int)round( dbl); }
	if (n>9999) { n=9999; }
	snprintf(buf, sizeof buf, "%s%s%04u\r", command, ((dbl<0)?"-":""), n);
	commbus->send(buf);
	if (dbl<0) { return -(signed int)n; }
	return (signed int)n;
}
