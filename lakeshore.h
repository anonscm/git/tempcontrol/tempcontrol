/*
  Name:        linkam.h
  Copyright:   UCCS CNRS 2016
  Author:      RJS
  Date:        24/04/23
  Description: module for controlling a Lake Shore 325 temperature controller
*/

#include "serial.h"
#include "abstract_regulator.h"

class lakeshore : public abstract_regulator {
	public:
		lakeshore(serial*);
		lakeshore(serial*, char channel);
		int comm_isopened(void);
		double ramp_and_wait(double final_sp, double ramp_rate, double wait_time);
		double set_sp(double temperature);
		double get_sp(void);
		double get_pv(void);
		double stop(void);
		~lakeshore(void);

	private:
		int communicate(const char*, char*, size_t);
		void req(const char*);
		double reqf(const char*);
		int check_sp_unit(void);
		double go_ramp(double, double);
		serial *commbus;
		char channel; // 'A' by default
		int unit; // 0: undefined / 1: Kelvin / 2: Celsius / 3 = Sensor Unit
};
